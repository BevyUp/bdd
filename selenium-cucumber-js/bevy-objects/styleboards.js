module.exports = {    
    /**
     * returns a promise that is called when the url has loaded and the body element is present
     * @param {string} url - url to load
     * @param {integer} waitInSeconds - number of seconds to wait for page to load
     * @returns {Promise} resolved when url has loaded otherwise rejects
     * @example
     *      helpers.loadPage('http://www.google.com');
     */    
    LoadStyleBoard: function (url, waitInSeconds) {
        // use either passed in timeout or global default
        var timeout = (waitInSeconds) ? (waitInSeconds * 1000) : DEFAULT_TIMEOUT;
        return new Promise((resolve, reject) => {
            driver.get(url).then(function () {
                signInButton = driver.findElement(By.xpath("//*[text()='Sign In']"));
                if (signInButton) {
                    styleboards.SignIn(waitInSeconds).then((signin_resp) => {
                        signin_resp.get(url).then(() => {
                            //var waitTill = new Date(new Date().getTime() + timeout/3);                            
                            signin_resp.wait(until.elementLocated(By.id('bup_board_name'), timeout))
                            //signin_resp.findElement(By.id('bup_board_name'), timeout)
                            .then(null, function (err) {                                
                                if (err.name === "NoSuchElementError")
                                    reject(`Styleboards not loaded in page after waiting [${timeout}]. Element not found in DOM: 'bup_board_name'`);
                                resolve(signin_resp);
                            });
                        });
                    });
                }
                else
                {
                    var waitTill = new Date(new Date().getTime() + timeout/3);                            

                    driver.findElement(By.id('bup_board_name'))
                    .then(null, function (err) {                                
                        if (err.name === "NoSuchElementError")
                            reject(`Styleboards not loaded in page after waiting [${timeout}]. Element not found in DOM: 'bup_board_name'`);
                        resolve(signin_resp);
                    });                   
                }
            });
        });
    },
    SignIn: function () {
        var this_config = qa_config.nordstrom_com;        
        return new Promise((resolve, reject) => {
            driver.findElement(By.xpath("//*[text()='Sign In']")) //modal
                .click().then(() => {
                    driver.findElement(By.xpath("//*[text()='Sign In']")) //submit
                        .click().then(() => {
                            driver.findElement(By.xpath("//input[@name='username']")).sendKeys(this_config.email).then(() => {
                                driver.findElement(By.xpath("//input[@name='password']")).sendKeys(this_config.password).then(() => {
                                    driver.findElement(By.xpath("//button[@type='submit']")).click().then(() => {                                        
                                        var waitTill = new Date(new Date().getTime() + this_config.timeout_SignIn);
                                        while (waitTill > new Date()) { }
                                        resolve(driver);
                                    });
                                })
                            });
                        });
                });
        });
    }
};
