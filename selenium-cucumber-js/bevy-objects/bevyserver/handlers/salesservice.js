module.exports = {
    reportOrder: function (req, res) {
        if (req.method == 'POST') {
            var body = '';
        }
        else {
            res.writeHead(404, { 'Content-Type': 'text/html; charset=utf-8' });
            res.end('404 - Handler Not Found. Try POST');
        }

        req.on('data', function (data) {
            body += data;
        });

        req.on('end', function () {
            var order = JSON.parse(body);
            var resp = require(`../responses/${order.orderId}.json`)
            //Prepare Response...
            resp.items = _.each(resp.items, (i) => { i.lastUpdatedTimestamp = order.timestamp; }); //replace null values in response
            //Send Response 
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify(resp));
        });
    },
    updateOrder: function (req, res) {
        if (req.method == 'PUT') {
            var body = '';
        }
        else {
            res.writeHead(404, { 'Content-Type': 'text/html; charset=utf-8' });
            res.end('404 - Handler Not Found. Try PUT');
        }

        req.on('data', function (data) {
            body += data;
        });
        req.on('end', function () {
            var order = JSON.parse(body);
            var resp = require(`../responses/${order.orderId}.json`)
            //Prepare Response...
            resp.items = _.each(resp.items, (i) => {
                i.lastUpdatedTimestamp = order.timestamp; //replace null values in response
                i.status = _.find(order.items, (o) => { return o.lineItemId == i.lineItemId });//ackowledge status update
            });
            //Send Response
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.end(JSON.stringify(resp));
        });
    }
}