const http = require('http');
const https = require('https');
const fs = require('fs');

function start_http(port, requestHandler) {
    var server = http.createServer().listen(port)
    server.on('request', requestHandler);
    //handle errors
    console.log('start_http... port:' + port);
};

function start_https(port, requestHandler) {
    var options = {
        key: fs.readFileSync('./bevy-objects/bevyserver/cert/key1.pem').toString(),
        cert: fs.readFileSync('./bevy-objects/bevyserver/cert/cert1.pem').toString()
    }
    https.createServer(options, requestHandler).listen(port);
    //handle errors
    console.log('start_http... port:' + port);
};

function get_handler(request_url) {
    var script = `${request_url.split('?')[0]}`;
    script = script.replace(/\//g, "_");
    script = script.split('_')[1] + '.js';
    script = require('./handlers/' + script);
    return script;
};

var services = {    
    http_SalesService: function (config) {
        const requestHandler = (request, response) => {
            var handlers = get_handler(request.url);
            if (request.url.indexOf('/salesservice/reportOrder') == 0) {
                handlers.reportOrder(request, response)
            }
            else if (request.url.indexOf('/salesservice/updateOrder') == 0) {
                handlers.updateOrder(request, response)
            }
            else {
                response.writeHead(404, {'Content-Type': 'text/html; charset=utf-8'});
                response.end('404 - Handler Not Found. [http_SalesService]');
            }
        };

        start_http(config.port, requestHandler);
    },
    https_SalesService: function (config) {
        const requestHandler = (request, response) => {
            var handlers = get_handler(request.url);
            if (request.url.indexOf('/salesservice/reportOrder') == 0) {
                handlers.reportOrder(request, response)
            }
            else if (request.url.indexOf('/salesservice/updateOrder') == 0) {
                handlers.updateOrder(request, response)
            }
            else {
                response.writeHead(404, {'Content-Type': 'text/html; charset=utf-8'});
                response.end('404 - Handler Not Found. [https_SalesService]');
            }
        };

        start_https(config.port, requestHandler);
    },
    security_service: function () {
        console.log('some_service');
    }
};

module.exports = services;