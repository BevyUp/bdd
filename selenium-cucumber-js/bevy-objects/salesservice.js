module.exports = {
    ReportOrder: function (payload) {
        return new Promise((resolve, reject) => {
            var this_config = qa_config.salesservice.ReportOrder;
            var opts = this_config.http_options; //todo: validate if exists

            payload.correlationId = salesservice.GenerateGUID(payload.orderId);
            
            const now = JSON.stringify(new Date()).replace(/\"/g, '');
            payload.timestamp = now;
            
            _.each(payload.items, (item) => { item.updateTimestamp = now; });
            
            payload = JSON.stringify(payload);

            opts.headers = {
                Content_Type: opts.headers.Content_Type,
                Ocp_Apim_Subscription_Key: opts.headers.Ocp_Apim_Subscription_Key,
                Content_Length: Buffer.byteLength(payload)                
            }
            
            //console.log(`log: http ReportOrder [${this_config.endpoint}]`);
            ////return rest.http_request(JSON.stringify(payload, null, 2), opts)
            return rest.https_request(payload, opts)
                .then((rest_resp) => {
                    if (rest_resp && rest_resp.error)
                        reject(`err: api 'ReportOrder' [${opts.hostname}${opts.path}] response failed: '${rest_resp.error}'`);
                    else {
                        resolve(rest_resp);
                    }
                })
                .catch((rest_resp) => {
                    reject(rest_resp);
                });
        });
    },
    UpdateOrder: function (payload) {
        return new Promise((resolve, reject) => {
            var this_config = qa_config.salesservice.UpdateOrder;
            var opts = this_config.http_options; //todo: validate if exists

            payload.correlationId = salesservice.GenerateGUID(payload.orderId);
            
            const now = JSON.stringify(new Date()).replace(/\"/g, '');
            payload.timestamp = now;
            
            _.each(payload.items, (item) => { item.updateTimestamp = now; });
            
            payload = JSON.stringify(payload);

            opts.headers = {
                Content_Type: opts.headers.Content_Type,
                Ocp_Apim_Subscription_Key: opts.headers.Ocp_Apim_Subscription_Key,
                Content_Length: Buffer.byteLength(payload)                
            }
            
            //console.log(`log: http ReportOrder [${this_config.endpoint}]`);
            ////return rest.http_request(JSON.stringify(payload, null, 2), opts)
            return rest.https_request(payload, opts)
                .then((rest_resp) => {
                    if (rest_resp && rest_resp.error)
                        reject(`err: api 'ReportOrder' [${opts.hostname}${opts.path}] response failed: '${rest_resp.error}'`);
                    else {
                        resolve(rest_resp);
                    }
                })
                .catch((rest_resp) => {
                    reject(rest_resp);
                });
        });
    },
    GenerateGUID: function (orderId) {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        function s12() {
            return JSON.stringify(new Date()).replace(/-|:|\"|\.|/g, '').split("").reverse().join("").substring(0, 12)
        }

        function s8(orderId) {
            return orderId.substring(1, 5) + orderId.substring(1, 5).split("").reverse().join("");
        }

        return `${s8(orderId)}-${s4()}-${s4()}-${s4()}-${s12()}`;
    },
    DeleteOrderRecords: function (orderId) {
        return new Promise((resolve, reject) => {

            var this_config = qa_config.salesservice.DeleteOrderRecords;
            var query = this_config.storedProcedure.query;
            var param = this_config.storedProcedure.params[0];
            query = query.replace(param, orderId);

            return sql.sql_query(query, this_config.config)
                .then((sql_resp) => { resolve(sql_resp); })
                .catch((sql_resp) => { reject(sql_resp); });
        });
    }
};
