/****** Object:  StoredProcedure [dbo].[spdeleteSalesServiceOrder]    Script Date: 4/5/2018 10:46:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================
-- Author:		 
-- Create Date:		
-- Description:		
-- Parameters:
-- @TransactionId:		Order Id
-- Author:			Rafael Villota Canal
-- Modify Date:		04/03/2018
-- Description:		Deletes a transaction from all tables involved
--
-- NOTE:			THIS PROCEDURE IS NOT INTENDED TO BE IN THE PRODUCTION ENVIRONMENT
--					ONLY IN DOGFOOD FOR TESTING PURPOSES
-- ==================================================
ALTER   PROCEDURE [dbo].[spdeleteSalesServiceOrder] 
   -- IN Params
    @transactionId varchar(50)
AS BEGIN
SET NOCOUNT ON;
    DECLARE
      @ErrorMessage   varchar(2000)
     ,@ErrorSeverity  tinyint
     ,@ErrorState     tinyint
	 ,@Serverdate	  datetime
	 ,@PartnerId	  nvarchar(50) = 'nordstrom_o1'

BEGIN TRY
	SELECT @Serverdate = Serverdate FROM IncomingTransactions WHERE PartnerId = @PartnerId AND TransactionId = @transactionId
	DELETE FROM IncomingTransactions WHERE PartnerId = @PartnerId AND TransactionId = @transactionId
	DELETE FROM IncomingTransactionsDetails WHERE TransactionId = @transactionId AND Serverdate = @Serverdate
	DELETE FROM BoardTransactionLog WHERE PartnerId = @PartnerId AND TransactionId = @transactionId AND Serverdate = @Serverdate
	DELETE FROM BoardTransactionLogDetails WHERE TransactionId = @transactionId AND Serverdate = @Serverdate
	DELETE FROM BoardTransactionLedger WHERE TransactionId = @transactionId AND Serverdate = @Serverdate
	DELETE FROM LineItemStatus WHERE TransactionId = @transactionId AND Serverdate = @Serverdate
	DELETE FROM AgentSessionsLogActivity WHERE ActivityType IN (5,13) AND PartnerId = @PartnerId AND JSON_VALUE(ActivityDetails, '$.TransactionId') = @transactionId
END TRY

BEGIN CATCH
    SET @ErrorMessage  = ERROR_MESSAGE()
    SET @ErrorSeverity = ERROR_SEVERITY()
    SET @ErrorState    = ERROR_STATE()
    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState)
END CATCH

END