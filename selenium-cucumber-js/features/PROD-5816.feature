Feature: PROD-5816 | Business request to add order status backordered 
  
Scenario: UpdateOrder updates items status from 'ordered' to 'backordered'
  Given that order "o5816.json" records where removed from storage
  #Given that board "b5816.json" has been opened
  When order "o5816.json" has been reported to BevyUp
  Then item "6185o-0" from previous step matches with agent "94361329"
	When order "o5816.json" is updated with "6185o-0" as "backordered" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "backorded"
  
Scenario: UpdateOrder updates items status from 'ordered' to 'canceled'
  Given that order "o5816.json" records where removed from storage
  #Given that board "b5816.json" has been opened
  When order "o5816.json" has been reported to BevyUp
  Then item "6185o-0" from previous step matches with agent "94361329"
	When order "o5816.json" is updated with "6185o-0" as "canceled" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "canceled"

Scenario: UpdateOrder updates items status from 'ordered' to 'shipped'
  Given that order "o5816.json" records where removed from storage
  #Given that board "b5816.json" has been opened
  When order "o5816.json" has been reported to BevyUp
  Then item "6185o-0" from previous step matches with agent "94361329"
	When order "o5816.json" is updated with "6185o-0" as "shipped" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "shipped"

Scenario: UpdateOrder updates items status from 'ordered' to 'returned'
  Given that order "o5816.json" records where removed from storage
  #Given that board "b5816.json" has been opened
  When order "o5816.json" has been reported to BevyUp
  Then item "6185o-0" from previous step matches with agent "94361329"
	When order "o5816.json" is updated with "6185o-0" as "returned" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "returned"
  #Validate Ledger (shipped + returned)
  
Scenario: UpdateOrder updates items status from 'ordered' through 'backordered/canceled'
  Given that order "o5816.json" records where removed from storage
  #Given that board "b5816.json" has been opened
  When order "o5816.json" has been reported to BevyUp
  Then item "6185o-0" from previous step matches with agent "94361329"
	When order "o5816.json" is updated with "6185o-0" as "backordered" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "backordered"
  When order "o5816.json" is updated with "6185o-0" as "canceled" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "canceled"

Scenario Outline: UpdateOrder updates items status from 'ordered' through 'backordered/shipped'
  Given that order "o5816.json" records where removed from storage
  #Given that board "b5816.json" has been opened
  When order "o5816.json" has been reported to BevyUp
  Then item "6185o-0" from previous step matches with agent "94361329"
	When order "o5816.json" is updated with "6185o-0" as "backordered" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "backordered"
  When order "o5816.json" is updated with "6185o-0" as "shipped" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "shipped"

Scenario: UpdateOrder updates items status from 'ordered' through 'backordered/returned'
  Given that order "o5816.json" records where removed from storage
  #Given that board "b5816.json" has been opened
  When order "o5816.json" has been reported to BevyUp
  Then item "6185o-0" from previous step matches with agent "94361329"
	When order "o5816.json" is updated with "6185o-0" as "backordered" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "backordered"
  When order "o5816.json" is updated with "6185o-0" as "returned" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "returned"

 Scenario: UpdateOrder updates items status from 'ordered' through 'shipped/returned'
  Given that order "o5816.json" records where removed from storage
  #Given that board "b5816.json" has been opened
  When order "o5816.json" has been reported to BevyUp
  Then item "6185o-0" from previous step matches with agent "94361329"
	When order "o5816.json" is updated with "6185o-0" as "shipped" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "shipped"
  When order "o5816.json" is updated with "6185o-0" as "returned" 
  Then item "6185o-0" from previous step matches with agent "94361329"
  Then SalesApp renders item "6185o-0" as "returned"

  
#   Scenario Outline: UpdateOrder updates valid item status on  'existing/reported' items
#     Given that order <report_order_json> has been reported to BevyUp 
# 	When order <report_order_json> is updated with <lineItem_Id> as <original_status>
# 	When order <report_order_json> is updated with <lineItem_Id> as <new_status>
#     Then <lineItem_Id> is matched as <new_status> 
# 	Then SalesApp renders item <lineItem_Id> as <new_status> 
	
# 	Examples:
#     | original_status	| report_order_json	| lineItem_Id	| new_status	|
#     | backordered		| r5816_2.json		| 5816_2-0		| canceled		|
# 	| backordered		| r5816_2.json		| 5816_2-1		| shipped		|
# 	| backordered		| r5816_2.json		| 5816_2-2		| returned		|
# 	| shipped			| r5816_2.json		| 5816_2-3		| returned		|
	
#   Scenario: UpdateOrder reports error for invalid status on 'ordered' items
#     Given that order "r5816_3.json" has been reported to BevyUp
# 	When order "r5816_3.json" is updated with "5816_4-0" as "ordered"
#     Then UpdateOrder response contains error "SOME_ERROR"
# 	Then SalesApp renders item "5816_4-0" as "ordered"
	
#   Scenario Outline: UpdateOrder reports error for invalid status on 'existing/reported' items  
#     Given that order "r5816_4.json" has been reported to BevyUp 
# 	When order "r5816_4.json" is updated with <lineItem_Id> as <original_status>
# 	When order "r5816_4.json" is updated with <lineItem_Id> as <new_status>
#     Then UpdateOrder response contains error <error_message>
# 	Then SalesApp renders item <lineItem_Id> as <original_status>
	
# 	Examples:
#     | original_status	| lineItem_Id	| error_message	| new_status	|
#     | backordered		| 5816_4-0		| "SOME_ERROR"	| ordered		|
# 	| backordered		| 5816_4-1		| "SOME_ERROR"	| backordered	|
# 	| canceled			| 5816_4-2		| "SOME_ERROR"	| ordered		|
# 	| canceled			| 5816_4-3		| "SOME_ERROR"	| backordered	|
# 	| canceled			| 5816_4-4		| "SOME_ERROR"	| canceled		|
# 	| canceled			| 5816_4-5		| "SOME_ERROR"	| shipped		|
# 	| canceled			| 5816_4-6		| "SOME_ERROR"	| returned		|
# 	| shipped			| 5816_4-7		| "SOME_ERROR"	| ordered		|
# 	| shipped			| 5816_4-8		| "SOME_ERROR"	| backordered	|
# 	| shipped			| 5816_4-9		| "SOME_ERROR"	| canceled		|
# 	| shipped			| 5816_4-10		| "SOME_ERROR"	| shipped		|
# 	| returned			| 5816_4-11		| "SOME_ERROR"	| ordered		|
# 	| returned			| 5816_4-12		| "SOME_ERROR"	| backordered	|
# 	| returned			| 5816_4-13		| "SOME_ERROR"	| canceled		|
# 	| returned			| 5816_4-14		| "SOME_ERROR"	| shipped		|
# 	| returned			| 5816_4-15		| "SOME_ERROR"	| returned		|

#   Scenario Outline: ReportOrder reports error for invalid status items  
#     Given that order <report_order_json> has been reported to BevyUp
# 	Then ReportOrder response contains error <error_message>
	
# 	Examples:
#     | invalid_status	| report_order_json	| error_message	|
#     | backordered		| r5816_5b.json		| "SOME_ERROR"	|
# 	| canceled			| r5816_5c.json		| "SOME_ERROR"	|
# 	| shipped			| r5816_5s.json		| "SOME_ERROR"	|
# 	| returned			| r5816_5r.json		| "SOME_ERROR"	|
	
#   Scenario: ReportOrder successfully matches 'ordered' items  
#     Given that order "r5816_6.json" has been reported to BevyUp	
#     Then order item "5816_6-0" is matched
# 	Then order item "5816_6-1" is not matched
# 	Then SalesApp renders item "5816_6-0" as "ordered"
# 	Then SalesApp does not render item "5816_6-1"
