module.exports = {

    /**
     * returns a promise that is called when the url has loaded and the body element is present
     * @param {string} url - url to load
     * @param {integer} waitInSeconds - number of seconds to wait for page to load
     * @returns {Promise} resolved when url has loaded otherwise rejects
     * @example
     *      helpers.loadPage('http://www.google.com');
     */
    http_request: function (payload, opts) {
        return new Promise((resolve, reject) => {
            const req_data = payload;
            const req_options = opts;
            
            var http_req = http.request(req_options, function (res) {
                var resp_data = '';

                res.on('data', (chunk) => { resp_data += chunk; });
                res.on('error', (e) => {
                    reject(`err: http_request [${req_options.hostname}/${req_options.path}] did not complete succesfully (on.error): '${e.message}`);
                });
                res.on('end', () => {
                    resp_data = JSON.parse(resp_data)
                    if (resp_data && resp_data.statusCode && resp_data.statusCode != 200)
                        reject(`err: http_request [${req_options.hostname}/${req_options.path}] response was '${resp_data.statusCode}' (on.end): '${resp_data.message}'`);
                    resolve(resp_data);
                });
            });

            http_req.write(req_data);
            http_req.end();
        });
    },
    https_request: function (payload, opts) {
        return new Promise((resolve, reject) => {
            const req_data = payload;
            const req_options = opts;
            process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
            var http_req = https.request(req_options, function (res) {
                var resp_data = '';

                res.on('data', (chunk) => { resp_data += chunk; });
                res.on('error', (e) => {
                    reject(`err: http_request [${req_options.hostname}/${req_options.path}] did not complete succesfully (on.error): '${e.message}`);
                });
                res.on('end', () => {
                    resp_data = JSON.parse(resp_data)
                    if (resp_data && resp_data.statusCode && resp_data.statusCode != 200)
                        reject(`err: http_request [${req_options.hostname}/${req_options.path}] response was '${resp_data.statusCode}' (on.end): '${resp_data.message}'`);
                    resolve(resp_data);
                });
            });

            http_req.write(req_data);
            http_req.end();
        });
    }
};
