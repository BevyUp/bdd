module.exports = {

    /**
     * returns a promise that is called when the url has loaded and the body element is present
     * @param {string} url - url to load
     * @param {integer} waitInSeconds - number of seconds to wait for page to load
     * @returns {Promise} resolved when url has loaded otherwise rejects
     * @example
     *      helpers.loadPage('http://www.google.com');
     */
    sql_query: function (query, config) {
        return new Promise((resolve, reject) => {           
            var connection = new SqlConnection(config);
            connection.config.userName = config.user;
            connection.config.database = config.database;
            connection.on('connect', function (err) {
                if (err) { reject(err); }
                else {
                    var result = "";
                    var json_res = "";

                    request = new SqlRequest(query, function (err) {
                        if (err) { throw(err); }
                    });
                    request.on('row', function (columns) {
                        columns.forEach(function (column) {
                            if (json_res)
                                json_res = json_res + ", "
                            json_res += `"${column.metadata.colName}" : "${column.value}" `;
                        });

                        json_res = `{ ${json_res} }`;
                        if (result)
                            result = result + ", "
                        result += json_res;
                        json_res = ""
                    });

                    request.on('doneProc', function (rowCount, more, rows) {       //investigate other events                        
                        resolve(JSON.parse(`[ ${result} ]`));
                    });

                    connection.execSql(request);
                }
            });
        });
    }
};
