/**
 * PROD-5816
 */
module.exports = function () {

    this.Given(/^that order "([^"]*)" records where removed from storage$/, function (report_order_json, callback) {
        var oJSON = helpers.readFromContent(report_order_json);
        // console.log(`log: input - Board [${report_order_json}]`);
        if (!oJSON || !oJSON.orderId) { callback(`err: Failed to delete records for order. No valid 'orderId' was found`, 'fail'); }

        salesservice.DeleteOrderRecords(oJSON.orderId)
            .then((resp) => {
                ctx.pushStepResult();
                callback(null, 'pass');
            }).catch((rej) => {
                callback(`err: Failed to delete records for order '${oJSON.orderId}'. ` + rej, 'fail');
            });
    });

    this.When(/^board "([^"]*)" is opened$/, function (board_config, callback) {
        // console.log(`log: input - Board [${board_config}]`);
        var oJSON = helpers.readFromContent(board_config);
        styleboards.LoadStyleBoard(oJSON.url)
            .then((resp) => {
                ctx.pushStepResult(resp);
                callback(null, 'pass');
            }).catch((rej) => {
                callback(rej, 'fail');
            });
    });

    this.Given(/^that board "([^"]*)" has been opened$/, function (board_config, callback) {
        // console.log(`log: input - Board [${board_config}]`);
        var oJSON = helpers.readFromContent(board_config);
        styleboards.LoadStyleBoard(oJSON.url)
            .then((resp) => {
                ctx.pushStepResult(resp);
                callback(null, 'pass');
            }).catch((rej) => {
                callback(rej, 'fail');
            });
    });

    this.When(/^order "([^"]*)" has been reported to BevyUp$/, function (report_order_json, callback) {
        var oJSON = helpers.readFromContent(report_order_json);
        salesservice.ReportOrder(oJSON)
            .then((ro_res) => {
                ctx.pushStepResult(ro_res);
                callback(null, 'pass');
            })
            .catch((rej) => {
                callback(rej, 'fail');
            });
    });

    this.When(/^order "([^"]*)" is updated with "([^"]*)" as "([^"]*)"$/, function (report_order_json, lineItem_Id, new_status, callback) {
        var oJSON = helpers.readFromContent(report_order_json);
        oJSON.items = _.map(oJSON.items, (item, k) => {
            item.status = item.lineItemId == lineItem_Id ? new_status : item.status; return item;
        });

        salesservice.UpdateOrder(oJSON)
            .then((uo_res) => {
                ctx.pushStepResult(uo_res);
                callback(null, 'pass');
            })
            .catch((rej) => {
                callback(rej, 'fail');
            });
    });

    this.Then(/^item "([^"]*)" from step '(\d+)' matches with agent "([^"]*)"$/, function (lineItem_Id, stepRef, expectedAgent, callback) {
        var actualStepResult = ctx.getStepResult(stepRef);
        expect(actualStepResult.error).to.be.null;
        expect(actualStepResult.matched).to.be.true;
        var actualAgent = _.find(actualStepResult.items, (i) => { return i.lineItemId == lineItem_Id }).matchedAgent.employeeId;
        expect(actualAgent).to.be.eql(expectedAgent);
        ctx.pushStepResult();
        callback(null, 'pass');
    });

    this.Then(/^item "([^"]*)" from previous step matches with agent "([^"]*)"$/, function(lineItem_Id, expectedAgent, callback) {
        var actualStepResult = ctx.previousStepResult();
        expect(actualStepResult.error).to.be.null;
        expect(actualStepResult.matched).to.be.true;
        var actualAgent = _.find(actualStepResult.items, (i) => { return i.lineItemId == lineItem_Id }).matchedAgent.employeeId;
        expect(actualAgent).to.be.eql(expectedAgent);
        ctx.pushStepResult();
        callback(null, 'pass');
      });

    this.Then(/^SalesApp renders item "([^"]*)" as "([^"]*)"$/, function (arg1, arg2, callback) {

        // Write code here that turns the phrase above into concrete actions
        callback(null, 'pending');
    });
};
